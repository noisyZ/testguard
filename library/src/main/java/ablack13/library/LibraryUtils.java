package ablack13.library;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * Created by ablack13 on 21.02.17.
 */

class LibraryUtils {
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static String getLibraryMessage(Context context) {
        return context.getString(R.string.library_name);
    }
}
